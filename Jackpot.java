public class Jackpot {
	public static void main(String[] args) {
		System.out.println("Welcome to Jackpot! Let's start a game.");
		Board board = new Board();
		boolean gameOver = false;
		int numOfTilesClosed = 0;
		do {
			String tiles = board.toString();
			System.out.println(tiles);
			gameOver = board.playATurn();
			numOfTilesClosed ++;
		} while (!gameOver);
		if (numOfTilesClosed > 6) {
			System.out.println("Congrats! you reached the jackpot and won! you closed a total of "+numOfTilesClosed+" tiles!");
		} else {
			System.out.println("Oh no! you didn't close at least 7 tiles! better luck next time. (you closed a total of "+numOfTilesClosed+" tiles).");
		}
	}
}