import java.util.Random;
public class Dice {
	private int faceValue;
	private int random; 
	Random rand = new Random();
	
	public Dice() {
		this.faceValue = 1;
		this.random = rand.nextInt(6)+1;
		// i am very confused by the instructions for this, so i ended up importing the random util
	}
	public int getFaceValue() {
		return this.faceValue;
	}
	public void roll() {
		// this would probably make more sense if i understood instructions 2 - 3
		this.random = rand.nextInt(6)+1;
		this.faceValue = this.random;
	}
	public String toString() {
		String numString = this.faceValue+"";
		return numString;
	}
}