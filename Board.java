public class Board {
	private boolean[] tiles;
	Dice[] die = new Dice[2];
	
	public Board() {
		die[0] = new Dice();
		die[1] = new Dice();
		tiles = new boolean[12];
	}
	
	public String toString() {
		String tilesValue = "";
		for (int i = 0; i < tiles.length; i++) {
			if(tiles[i]) {
				tilesValue += "X";
			} else {
				tilesValue += i+1;
			}
		}
		return tilesValue;
	}
	public boolean playATurn() {
		die[0].roll();
		die[1].roll();
		String die1 = die[0].toString();
		String die2 = die[1].toString();
		int valueDie1 = Integer.parseInt(die1);
		int valueDie2 = Integer.parseInt(die2);
		
		System.out.println("the value of the rolled dice are "+die1+" and "+die2+".");
		int sumOfDice = valueDie1+valueDie2;
		if (!tiles[sumOfDice-1]) {
			tiles[sumOfDice-1] = true;
			System.out.println("closing tile equal to sum: "+sumOfDice);
			return false;
		} else if (!tiles[valueDie1-1]) {
			tiles[valueDie1-1] = true;
			System.out.println("closing tile with the same value as die one: "+die1);
			return false;
		} else if (!tiles[valueDie2-1]) {
			tiles[valueDie2-1] = true;
			System.out.println("closing tile with the same value as die one: "+die2);
			return false;
		} else {
			System.out.println("All the tiles for these values are shut");
			return true;
		}
		
	}
	
}